package main

import (
	"fmt"
	"net/http"
)

func main() {

	fs := http.FileServer(http.Dir(""))
	http.Handle("/", fs)
	fmt.Println("Server is listening...")
	http.ListenAndServe(":8181", nil)
}
